#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

int calculate(int a, int b, char op) {
	if (op == '+')
		return a + b;
	else if (op == '-')
		return a - b;
	else if (op == '*')
		return a * b;
	else if (op == '/')
		return a / b;
}

int main(void){
	int m;
	scanf("%d", &m);
	
	int x, y, current=0;
	char op;
	int count = 3;

	scanf("%d %d %c", &x, &y,&op);

	current = calculate(x, y, op);

	while(count<m) {
		scanf("%d %c", &x,&op);
		count += 2;
		current = calculate(current, x, op);
	}
	printf("\n%d", current);
}

