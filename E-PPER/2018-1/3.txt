#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

int main(void) {
	int n;
	scanf("%d", &n);
	int isPrime = 1;

	for (int i = 2; i < n; i++) {
		if (n%i == 0) {
			isPrime = 0;
			break;
		}
	}
	if (n == 1)
		isPrime = 0;

	printf("%d", isPrime);
}


____________________________________________________

package algorithm;

import java.util.*;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int n = in.nextInt();
		int isPrime = 1;
		
		for(int i=2; i<n;i++) {
			if(n%i == 0) {
				isPrime = 0;
				break;
			}
		}
		
		if(n == 1)
			isPrime = 0;
		
		System.out.println(isPrime);
	}
}
