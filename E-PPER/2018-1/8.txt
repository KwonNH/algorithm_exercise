#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

int main(void){
	char s[100];	
	char pattern[10];
	int pattern_int[10];

	int i;
	int index;

	gets_s(s);
	int len = strlen(s);
	int expand = len;

	gets_s(pattern);

	for (i = 0; i < 7; i++) {
		pattern_int[i] = int(pattern[i]) - 48;
	}

	if (len % 7 != 0){
		for (i = 0; i < 7 -  len% 7; i++) {
			s[len + i] = 'a' + i;
			expand++;
		}
	}
	
	len = expand;

	i = 0;
	int loop = 0;
	
	while (i <= len) {
		if (i % 7 == 0 && i!= 0)
			loop++;

		index = pattern_int[i%7]-1;
		
		printf("%c", s[loop*7+index]);
		
		i++;
		
	}
	
}

