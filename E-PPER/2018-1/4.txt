#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>

int different(int a, int b, int c) {
	int diff;

	if (a == b)
		diff = c;
	if (a == c)
		diff = b;
	if (b == c)
		diff = a;

	return diff;
}

int main(void) {
	int point[3][2] = { 0 };
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 2; j++) {
			scanf("%d", &point[i][j]);
		}
	}
	
	int x, y;

	x = different(point[0][0], point[1][0], point[2][0]);
	y = different(point[0][1], point[1][1], point[2][1]);
	
	printf("%d %d", x, y);

}

