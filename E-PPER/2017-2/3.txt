#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

int main(void) {
	char s[10001];
	int i = 0;
	int isP = 1;
	gets_s(s);

	int n = strlen(s);

	for (i; i < (int)n / 2; i++) {
		if (s[i] != s[n - i - 1])
			isP = 0;
	}

	if (isP == 0)
		printf("no");
	else
		printf("yes");
	
	

}


------------------------------------------


package algorithm;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		String str = in.nextLine();
		
		int n = str.length();
		
		String isP = "yes";
		
		for(int i=0;i<(int)(n/2);i++) {
			if(str.split("")[i].compareTo(str.split("")[n-i-1]) != 0)
				isP = "no";
		}
		
		System.out.println(isP);
		
	}
}
