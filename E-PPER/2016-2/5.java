package algorithm;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int date = in.nextInt();
		int num = in.nextInt();
		
		int current;
		int count = 0;
		
		for(int i=0;i<num;i++) {
			current = in.nextInt();
			if(current%10 == date%10)
				count++;
		}
		
		System.out.println(count);
		
	}
}