package algorithm;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int n = in.nextInt();
		String binary="";
		
		while(n>0) {
			binary += Integer.toString(n%2);
			n = n/2;
		}
		
		for(int i=binary.length()-1;i>=0;i--) {
			System.out.print(binary.charAt(i));
		}
		
		
	}
}