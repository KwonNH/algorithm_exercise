package algorithm;

import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		Stack<String> stack = new Stack<String>();
		String current;
		String str = in.nextLine();
		StringTokenizer token = new StringTokenizer(str);
		
		String result = "T";
		
		while(token.hasMoreTokens()) {
			current = token.nextToken();
			
			if(current == "(")
				System.out.println("==");
			
			if(current.equals("("))
				stack.push(current);
			else if(current.equals(")")) {
				if(stack.isEmpty()) {
					result="F";
					break;
				}
				else
					stack.pop();
			}
			
		}
		
		System.out.println(result);
	}
}