package algorithm;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		String str = in.nextLine();
		
		int open=0, close=0;
		String result;

		for(int i=0;i<str.length();i++) {
			if(str.charAt(i) == '(')
				open++;
			else if(str.charAt(i) == ')')
				close++;
		}
		
		if(open != close)
			result = "F";
		else {
			if(str.charAt(0)==')' || str.charAt(str.length()-1) == '(')
				result = "F";
			else
				result = "T";
		}
		
		System.out.println(result);
		
	}
}