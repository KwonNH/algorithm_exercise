package algorithm;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int n = in.nextInt();
		
		int count = 1;
		
		while(n!=1) {
			
			if(n%2 == 0)
				n = n/2;
			else
				n = n*3+1;		
			
			count++;
		}
		
		System.out.println(count);
	
		
	}
}