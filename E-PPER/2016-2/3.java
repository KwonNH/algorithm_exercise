package algorithm;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		String s = in.nextLine();
		
		String cycle = "T";
		
		for(int i=0;i<s.length()/2;i++) {
			if(s.charAt(i) != s.charAt(s.length()-i-1)) {
				cycle = "F";
				break;
			}		
		}
		
		System.out.println(cycle);
	}
}