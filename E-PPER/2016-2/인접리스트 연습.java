package algorithm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		int N = in.nextInt();
		int L = in.nextInt();
		
		String result = "T";
		
		ArrayList<ArrayList<Integer>> ad = new <ArrayList<Integer>> ArrayList();
		
		for(int i=0;i<N;i++) {
			ad.add(new<Integer> ArrayList());
		}
		
		for(int i=0;i<L;i++) {
			int t1, t2;
			t1 = in.nextInt();
			t2 = in.nextInt();
			
			ad.get(t1).add(t2);
			ad.get(t2).add(t1);
		}
		
		for(int i=0;i<N;i++) {
			Iterator<Integer> iter = ad.get(i).iterator();
			System.out.print(i);
            if(iter.hasNext()) System.out.print("-");
            while(iter.hasNext()) System.out.print(iter.next() + " ");
            System.out.println("");

			int count = 0;
			while(iter.hasNext()) {
				count++;
				if(i == iter.next() && count == 2) {
					result = "F";
					break;
				}
			}
		}
		
		System.out.println(result);
	}
}